package testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC003MergeLead extends ProjectMethods {
		
	@BeforeTest
	public void setData() {
		testCaseName = "TC003MergeLead";
		testDesc = "Merge Lead";
		author = "Jayashree";
		category = "smoke";
	}
	@Test
	public void mergeLead() throws InterruptedException {
		WebElement eleleads = locateElement("xpath", "(//div[@class='x-panel-header']/a)[2]");
		click(eleleads);
		
		WebElement eleMergeleads = locateElement("linktext", "Merge Leads");
		click(eleMergeleads);
		
		WebElement eleFromLead = locateElement("xpath", "//table[@class='twoColumnForm']/tbody/tr[1]/td[2]/a/img");
		click(eleFromLead);
		switchToWindow(1);
		
		WebElement eleFirst_name = locateElement("name", "firstName");
		type(eleFirst_name, "Jayashree");
		
		WebElement eleFind_Leads = locateElement("xpath", "(//button[@class='x-btn-text'])[1]");
		click(eleFind_Leads);
		
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class='x-grid3-row-table']/tbody/tr[1]/td/div/a"))).click();
		switchToWindow(0);
		Thread.sleep(1000);
		
		WebElement eleToLead = locateElement("xpath", "//table[@class='twoColumnForm']/tbody/tr[2]/td[2]/a/img");
		click(eleToLead);
		switchToWindow(1);
		
		WebElement eleFirst_name_toLead = locateElement("name", "firstName");
		type(eleFirst_name_toLead, "Ramya");
		
		WebElement eleFind_Leads_tolead_button = locateElement("xpath", "(//button[@class='x-btn-text'])[1]");
		click(eleFind_Leads_tolead_button);
		
		Thread.sleep(1000);
		WebDriverWait wait1 = new WebDriverWait(driver, 10);
		wait1.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class='x-grid3-row-table']/tbody/tr[1]/td/div/a"))).click();
		switchToWindow(0);
		
		WebElement eleMerge = locateElement("linktext", "Merge");
		click(eleMerge);
		
		acceptAlert();
		closeBrowser();
	}
}

