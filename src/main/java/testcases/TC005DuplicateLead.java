package testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC005DuplicateLead extends ProjectMethods  {
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC003DuplicateeLead";
		testDesc = "Create A duplicate Lead";
		author = "Jayashree";
		category = "smoke";
	}
	
	@Test
	public void duplicatelead() throws InterruptedException {

		WebElement eleleads = locateElement("linktext", "Leads");
		click(eleleads);
		WebElement eleFindLeads = locateElement("linktext", "Find Leads");
		click(eleFindLeads);
		WebElement eleEmail = locateElement("xpath", "(//span[@class='x-tab-strip-inner'])[3]");
		click(eleEmail);
		WebElement eleEnterEmail = locateElement("name", "emailAddress");
		type(eleEnterEmail, "kjayashree2007@gmail.com");
		WebElement eleFindLeads1 = locateElement("xpath", "(//button[@class='x-btn-text'])[7]");
		click(eleFindLeads1);
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class='x-grid3-row-table']/tbody/tr/td/div/a"))).click();
		WebElement eleduplicatelead = locateElement("linktext", "Duplicate Lead");
		click(eleduplicatelead);
		WebElement ele_verifyexacttext = locateElement("xpath", "//div[@class='x-panel-header sectionHeaderTitle']");
		verifyExactText(ele_verifyexacttext, "Duplicate Lead");
		WebElement eleCreateLeads = locateElement("xpath", "//input[@class='smallSubmit']");
		click(eleCreateLeads);
		WebElement ele_verifyexacttextName = locateElement("xpath", "//span[@id='viewLead_firstName_sp']");
		verifyExactText(ele_verifyexacttextName, "Jayashree");
		closeBrowser();
		
	}

}
