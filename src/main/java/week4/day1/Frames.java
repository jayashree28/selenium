package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class Frames {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		Alert alertRef=driver.switchTo().alert();
		String text = alertRef.getText();
		System.out.println(text);
		alertRef.sendKeys("Jayashree");
		alertRef.accept();
		
		
		String text2 = driver.findElementById("demo").getText();
		System.out.println(text2);
		if(text2.contains("Jayashree")) {
			System.out.println("Contains the text");
		}
	}

}
