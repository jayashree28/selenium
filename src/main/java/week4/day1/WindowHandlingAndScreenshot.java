package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandlingAndScreenshot {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/loginHome.jsf");
		driver.manage().window().maximize();
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("//ul[@id='topnavHP']/li[4]/a").click();
		//System.out.println(driver.getTitle());
		Set<String> allWindows = driver.getWindowHandles();
		List<String> lst = new ArrayList<>();
		lst.addAll(allWindows);
		driver.switchTo().window(lst.get(1));
		System.out.println(driver.getTitle());
		//String text=lst.get(0);
		//System.out.println(text);
		//System.out.println(lst.size());
		for (String old_window : lst) {
			driver.switchTo().window(old_window);
			//System.out.println(old_window);
			break;
		}
		driver.close();
	}
}
