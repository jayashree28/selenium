package week4.homework;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) throws InterruptedException{
		
		System.setProperty("webdriver.chrome.driver", "/home/oitw-praveen/Selenium/drivers/chromedriver");
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByXPath("//*[@id=\"login\"]/p[3]/input").click();
		
		driver.findElementByXPath("//*[@id=\"label\"]/a").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		
		driver.findElementByXPath("//table[@class='twoColumnForm']/tbody/tr[1]/td[2]/a/img").click();
		Set<String> allWindows = driver.getWindowHandles();
		List<String> lst = new ArrayList<>();
		lst.addAll(allWindows);
		driver.switchTo().window(lst.get(1));
		
		driver.findElementByName("firstName").sendKeys("Jayashree");
		driver.findElementByXPath("//button[@class='x-btn-text']").click();

		Thread.sleep(1000);
		WebElement table = driver.findElementByXPath("//table[@class='x-grid3-row-table']");
		List<WebElement> allRows=table.findElements(By.tagName("tr"));
		List<WebElement> eleColumns = allRows.get(0).findElements(By.tagName("td"));
		System.out.println(eleColumns.size());
		eleColumns.get(0).click();
		WebElement td =eleColumns.get(0);
		List<WebElement> td_divs=td.findElements(By.tagName("div"));
		WebElement tdFirstDiv = td_divs.get(0);
		System.out.println(tdFirstDiv);
		List<WebElement> td_div_child=td.findElements(By.tagName("a"));
		WebElement aLink = td_div_child.get(0);
		System.out.println(aLink);
		aLink.click();
	}
}
