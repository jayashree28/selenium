package week4.homework;

public class Pattern_printing {

	public static void main(String[] args) {
		int range=4, num=1;
		for(int i=1;i<=range;i++) {
			for(int j=1;j<=i;j++) {
				System.out.print(num+" ");
				//print(...)-prints the number in same line does not move the cursor to a new line.
				++num;
			}
			System.out.println();
			//println() moves the cursor to a new line.
		}

	}

}
