package excel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class ReadExcel {
	
	//@Test
	//public void LearnExcel() throws IOException {
		public static Object[][] LearnExcel() throws IOException {
		//go to workbook
		XSSFWorkbook wb =  new XSSFWorkbook("./data/CreateLead.xlsx");
		//go to first sheet
		XSSFSheet sheet = wb.getSheetAt(0);
		//get number of rows
		int rowCount = sheet.getLastRowNum();
		System.out.println(rowCount);
		//get number of cells
		int cellCount = sheet.getRow(0).getLastCellNum();
		Object[][] data = new Object[rowCount][cellCount];
		System.out.println(cellCount);
		for (int j = 1; j <=rowCount; j++) {
			XSSFRow row = sheet.getRow(j);
			for (int i = 0; i <cellCount; i++) {

				//System.out.println(row);
				XSSFCell cell = row.getCell(i);
				//System.out.println(cell);
				try {
					String value = cell.getStringCellValue();
					System.out.println(value);
					data[j-1][i] = value;
				} catch (NullPointerException e) {
					System.out.println("");
				}
			} 
		}
		wb.close();
		return data;
	}
	
}
