package zoom_car_testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class ZC001 {
	
	@Test
	public void launch_browser() throws InterruptedException{
		
			System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
			ChromeDriver driver = new ChromeDriver();
			driver.get("https://www.zoomcar.com/chennai");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);	
			driver.findElementByXPath("//div[@class='search-block']/a").click();
			driver.findElementByXPath("//div[@class='component-popular-locations']/div[2]").click();
			driver.findElementByXPath("//button[@class='proceed']").click();
			Date date = new Date();
			DateFormat sdf = new SimpleDateFormat("dd");
			String today = sdf.format(date);
			int tomorrow = Integer.parseInt(today)+1;
			System.out.println(tomorrow);
			driver.findElementByXPath("//div[@class='day' and contains(text(),'"+tomorrow+"')]").click();
			driver.findElementByXPath("//button[@class='proceed']").click();
			String date_picked = (driver.findElementByXPath("//div[@class='day picked ']")).getText();
			if(date_picked.contains(""+tomorrow)) {
				driver.findElementByXPath("//button[@class='proceed']").click();
			}
			Thread.sleep(3000);
			List<WebElement> allPrice=driver.findElementsByXPath("//div[@class='price']");
			int size = allPrice.size();		
			System.out.println(size);
			List<String> allPrice_array = new ArrayList<>();
			for (WebElement eachPrice : allPrice) {
				//System.out.println(eachPrice.getText().replaceAll("\\D",""));
				allPrice_array.add(eachPrice.getText().replaceAll("\\D",""));
			}
			String max = Collections.max(allPrice_array);
			System.out.println(max);
			//String car_name = null;
			////div[@class='details']/h3
			// //div[@class='details' and contains(text(),'"+car_name+"')]
			String car_name=driver.findElementByXPath("//div[contains(text(),'"+max+"')]/preceding::h3[1]").getText();
			System.out.println(car_name);
			driver.findElementByXPath("//div[contains(text(),'"+max+"')]/following::button[1]").click();
			driver.close();
			
			
	}
}
	



